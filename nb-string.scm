
(module nb-string (by-start-close-char by-nested-string str-ref str-frst-rst skip-chars digest-when-in digest-until-space)
  (import scheme)
  (import r7rs)
  (import srfi-13)
  
  (define (by-start-close-char1 str start end exclusive)
	"If successful produces a pair of strings which head is the enclosed string between start stop chars and the tail is the rest after the end character"
	(let ((start-pos (string-index str start)))
	  (letrec ((rec (lambda (num-nest curr-start-pos prev-stop)
					  (let ((next-start (string-index str start (+ curr-start-pos 1)))
							(end-pos (string-index str end prev-stop)))
						(if (and next-start (< next-start end-pos))
							(rec (+ num-nest 1) next-start (+ end-pos 1))
							(if (= num-nest 0)
								(if (and start-pos end-pos)
									(values (substring str (if exclusive (+ start-pos 1) start-pos)  (if exclusive end-pos (+ end-pos 1)))
											(substring str (+ end-pos 1)))
									(values "" ""))
								(if end-pos
									(rec (- num-nest 1) (+ curr-start-pos 1) end-pos)
									(values "" ""))))))))
		(rec 0 start-pos start-pos))))

  (define (by-start-close-char str start close)
	(let ((same (char=? start close)))
	  (let-values (((fst rst) (str-frst-rst str)))
		(letrec ((rec (lambda (curr-char rest num-nested start-found start-idx curr-idx)
						(if (null? curr-char)
							(values num-nested start-found start-idx curr-idx)
							(cond ((char=? curr-char start)
								   (if (and same start-found)
									   (values (substring str (+ start-idx 1) curr-idx) rest)
									   (let-values (((fst rst) (str-frst-rst rest)))
										 (if start-found
											 (rec fst rst (+ num-nested 1) start-found start-idx (+ curr-idx 1))
											 (rec fst rst num-nested #t curr-idx (+ curr-idx 1))))))
								  ((char=? curr-char close)
								   (if start-found
									   (if (= num-nested 0)
										   (values (substring str (+ start-idx 1) curr-idx) rest)
										   (let-values (((fst rst) (str-frst-rst rest)))
											 (rec fst rst (- num-nested 1) start-found start-idx (+ curr-idx 1))))
									   (let-values (((fst rst)  (str-frst-rst rest)))
										 (rec fst rst num-nested start-found start-idx (+ curr-idx 1)))))
								  (else
								   (let-values (((fst rst)  (str-frst-rst rest)))
									 (rec fst rst num-nested start-found start-idx (+ curr-idx 1)))))))))
		  (if (null? fst)
			  (values "" "")
			  (rec fst rst 0 #f 0 0))))))
	
							   
							    
						   
						
  
  (define (by-nested-string str)
	"Returns the first nested string in str(string) and the rest"
	(let* ((st-pos (string-index str #\"))
		   (end-pos
			(let ((end-pos-temp
				   (string-index str #\" (+ st-pos 1))))
			  (if end-pos-temp (+ end-pos-temp 1) end-pos-temp))))
	  (if (and st-pos end-pos)
		  (values (substring str (+ st-pos 1) (- end-pos 1)) (substring str end-pos))
		  (values "" ""))))


  (define (str-ref str idx)
	"string-ref using string-ports"
	(let ((in (open-input-string str)))
	  (letrec ((recur (lambda (curr-idx)
						(let ((c (read-char in)))
						  (cond ((eof-object? c) "s")
								 ((= idx curr-idx) c)
								 (else (recur (+ curr-idx 1))))))))
		(recur 0))))
  
  (define (str-frst-rst str)
	"Takes first and rest from str (string)"
	(let ((lgth (string-length str)))
	  (cond ((= lgth 0) (values '() ""))
			((= lgth 1) (values (str-ref str 0) ""))
			(#t          (values (str-ref str 0) (substring str 1))))))
  
  (define (skip-chars str chars)
	"Skips characters specified in chars"
	(letrec ((rec (lambda (str cs)
					(let-values (((fst rst) (str-frst-rst str)))  
					  (if (null? fst)
						  fst	   
						  (if (member fst cs)
							  (rec rst cs)
							  str))))))
	  (rec str chars)))
  
   (define (digest-until-space str)
	 "Collects characters as long as it is not a white space"
	(letrec ((rec (lambda (str digested)
					(let-values (((fst rst) (str-frst-rst str)))  
					  (if (null? fst)
						  (values (list->string (reverse digested)) str)
						  (if (char=? fst #\space)
							  (values (list->string (reverse digested)) str)
							  (rec rst (cons fst digested))))))))
	  (rec str '())))

   
  
  (define (digest-when-in str digestables)
	"Collects characters as long as they match any in digestables"
	(letrec ((rec (lambda (str dgs digested)
					(let-values (((fst rst) (str-frst-rst str)))  
					  (if (null? fst)
						  (values (list->string (reverse digested)) str)
						  (if (member fst dgs)
							  (rec rst dgs (cons fst digested))
							  (values (list->string (reverse digested)) str)))))))
	  (rec str digestables '())))

  
  )
