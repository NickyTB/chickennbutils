;; -*- geiser-scheme-implementation: 'chicken -*-
(module nb-json (json get-val js-array get-kv-pair js-parse get-val-convert get-val-lazy get-by-path json1)
  (import scheme chicken nb-string)
  (use r7rs)
  (use srfi-13)

  (define json "{ \"Name\" : \"Lasse\", \"Age\" : 33 , \"Langs\" : [ {\"Name\" : \"Racket\", \"Native\" : false}, { \"Name\" : \"C\", \"Native\" : true } ] , \"Anithyn\" : Null, \"Adress\" : { \"Street\" : \"Lassegatan\"}}, \"IsTrue\" : true, \"IsFalse\" : false")

  (define json1 "{
    \"glossary\": {
        \"title\": \"example glossary\",
		\"GlossDiv\": {
            \"title\": \"S\",
			\"GlossList\": {
                \"GlossEntry\": {
                    \"ID\": \"SGML\",
					\"SortAs\": \"SGML\",
					\"GlossTerm\": \"Standard Generalized Markup Language\",
					\"Acronym\": \"SGML\",
					\"Abbrev\": \"ISO 8879:1986\",
					\"GlossDef\": {
                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",
						\"GlossSeeAlso\": [\"GML\", \"XML\"]
                    },
					\"GlossSee\": \"markup\"
                }
            }
        }
    }
}")

(define number-chars '(#\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\0 #\.))

(define (get-key json)
  (let ((str (skip-chars json '(#\space #\,))))
	(if (string-contains str "\"")
		(by-nested-string str)
		(values "" ""))))

(define (get-val-convert json)
  (if (string=? json "")
	  (values "" "")
	  (let* ((str (skip-chars json '(#\space #\:)))
			 (fst (str-ref str 0)))
		(cond ((char-numeric? fst)
			   (let-values (((num-str rest) (digest-when-in str number-chars)))
				 (values (string->number num-str) rest)))
			  ((char=? fst #\") (by-nested-string str))
			  ((or (char=? fst #\n) (char=? fst #\N))
			   (values "Null" (substring str 1)))
			  ((char=? fst #\[)
			   (let-values (((js-arr rest) (by-start-close-char str #\[ #\] #t)))
				 (values (js-array js-arr) rest)))
			  ((char=? fst #\{)
			   (let-values (((js-obj rest) (by-start-close-char str #\{ #\} #f)))
				 (values (js-parse js-obj 'convert) rest)))
			  (else            "que")))))

(define (get-val-lazy json)
  (if (string=? json "")
	  (values "" (delay "") "")
	  (let* ((str (skip-chars json '(#\space #\:)))
			 (fst (str-ref str 0)))
		(cond ((char-numeric? fst)
			   (let-values (((num-str rest) (digest-when-in str number-chars)))
				 (values num-str (delay (string->number num-str)) rest)))
			  ((char=? fst #\")
			   (let-values (((js-str rest)(by-nested-string str)))
				 (values js-str (delay js-str) rest)))
			  ((or (char=? fst #\n) (char=? fst #\N))
			   (values "Null" (delay '()) (substring str 1)))
			  ((char=? fst #\[)
			   (let-values (((js-arr rest) (by-start-close-char str #\[ #\] #t)))
				 (values js-arr (delay (js-array js-arr)) rest)))
			  ((char=? fst #\{)
			   (let-values (((js-obj rest) (by-start-close-char str #\{ #\} #f)))
				 (values js-obj (delay (js-parse js-obj 'convert)) rest)))
			  ((char=? fst #\t)
			   (values "true" (delay #t) (substring str 1)))
			  ((char=? fst #\f)
			   (values "false" (delay #f) (substring str 1)))
			  (else            "que")))))

(define (get-val json)
  (if (string=? json "")
	  (values "" "")
	  (let* ((str (skip-chars json '(#\space #\:)))
			 (fst (str-ref str 0)))
		(cond ((char-numeric? fst)
			   (let-values (((num-str rest) (digest-when-in str number-chars)))
				 (values num-str rest)))
			  ((char=? fst #\") (by-nested-string str))
			  ((or (char=? fst #\n) (char=? fst #\N))
			   (values "Null" (substring str 1)))
			  ((char=? fst #\[)
			   (let-values (((js-arr rest) (by-start-close-char str #\[ #\] #t)))
				 (values js-arr rest)))
			  ((char=? fst #\{)
			   (let-values (((js-obj rest) (by-start-close-char str #\{ #\} #f)))
				 (values js-obj rest)))
			  (else            "que")))))



(define (js-array json)
  (letrec ((rec (lambda (json acc)
				  (let ((str (skip-chars json '(#\space #\,))))
					(if (string=? str "")
						(list->vector acc)
						(let-values (((val rest) (get-val-convert str)))
						  (if (and (null? val) (string=? rest ""))
							  (list->vector acc)
							  (rec rest (cons val acc)))))))))
	(rec json '())))

(define (get-kv-pair1 json convert)
  (let-values (((k r1) (get-key json)))
	(if (not (string=? k ""))
		(if convert	
			(let-values (((v pr r2) (get-val-lazy r1)))
			  (values (cons k (force pr)) r2))
			(let-values (((v pr r2) (get-val-lazy r1)))
			  (values (cons k v) r2)))
		(values '() ""))))

(define (get-kv-pair json option)
  "Gets the first key value pair from json and returns that with the rest of the json after extracting the key value pair.
   Option 'convert indicates that the values are converted to their corresponding scheme type, 
   'noconvert keeps the values as strings and 'lazy returns a promise of a converted value."
  (let-values (((k r1) (get-key json)))
	(if (not (string=? k ""))
		(cond ((eq? option 'convert) (let-values (((v pr r2) (get-val-lazy r1))) (values (cons k (force pr)) r2)))
			  ((eq? option 'noconvert) (let-values (((v pr r2) (get-val-lazy r1))) (values (cons k v) r2)))
			  ((eq? option 'lazy) (let-values (((v pr r2) (get-val-lazy r1))) (values (cons k pr) r2)))
			  (else (error "Hmm: " option)))
		(values '() ""))))

(define (js-parse json option)
  "Returns the parsed json. Option is either 'convert to convert the json values to corresponding scheme values,
   or 'noconvert that keeps the values as strings, or 'lazy to return promises of the converted json values."
  (letrec ((rec (lambda (str acc)
				  (if (string=? str "")
					  acc
					  (let-values (((pair rest) (get-kv-pair str option)))
						(if (and (null? pair) (string=? rest ""))
							acc
							(rec rest (cons pair acc))))))))
	(rec json '())))

(define (get-by-path json path)
  "Gets the json value from json string by the path specified by the list of string names(keys) in path"
  (letrec ((rec (lambda (js curr-path)
				  (let-values (((pair rest) (get-kv-pair js 'lazy)))
					(if (string=? rest "")
						""
						(let ((key (car pair)))
						  (if (string-ci=? key (car curr-path))
							  (if (null? (cdr curr-path))
								  (values key (force (cdr pair)))
								  (rec rest (cdr curr-path)))
							  (rec rest curr-path))))))))
	(rec json path)))


)
